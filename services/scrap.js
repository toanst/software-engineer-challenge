const axios = require('axios');
const cheerio = require('cheerio');

/**
 * Get HTML content from url input
 * @param {string} url
 * @return {Promise<string>} string HTML content
 */
async function getContentFromUrl(url) {
  return axios
    .get(url)
    .then((response) => response.data)
    .catch((error) => {
      // Throw a shorter error message
      throw new Error(error.message);
    });
}

/**
 * Get data from HTML content
 * @param {string} content HTML content
 * @return {Promise<number[]>} Number array
 */
function getDataFromHTMLContent(content) {
  // Find a numeric column, for simply assuming numeric columns has a header is "Mark"
  const result = [];
  const $ = cheerio.load(content);
  // Find the table, with a header containing "Mark"
  const headerMarkElement = $('table tr th:contains("Mark")');
  if (headerMarkElement.length < 1) {
    throw new Error('Data from HTML not found');
  }
  // Find the index of "Mark" column
  const columnIndex = $(headerMarkElement[0]).closest('th').index();
  // Get all values of "Mark" column
  let tableRowCurr = $(headerMarkElement[0]).closest('tr').next();
  while (tableRowCurr.length === 1) {
    const rawValue = $(tableRowCurr.children()[0]).text();
    // Use regex to get the number value (first matching number)
    const valueMatch = rawValue.match(/[0-9.]+/);
    if (valueMatch) {
      const value = +valueMatch[0];
      result.push(value);
    } else {
      throw new Error('One or more value in Mark column is not a number');
    }
    tableRowCurr = tableRowCurr.next();
  }

  return result;
}

/**
 * Get Data (number array) from the URL to draw the chart
 * @param {string} url
 * @return {Promise<number[]>} Number array
 */
async function getDataFromUrl(url) {
  const content = await getContentFromUrl(url);
  const data = getDataFromHTMLContent(content);
  if (!data || data.length === 0) {
    throw new Error('Data is empty');
  }
  return data;
}

module.exports = {
  getContentFromUrl,
  getDataFromHTMLContent,
  getDataFromUrl,
};
