const { createLineChart } = require('./graph');

describe('createLineChart', () => {
  it('Data is not a number array', async () => {
    await expect(createLineChart([1, 'a', '1.1', 2])).rejects.toThrow(
      'Data is not a number array',
    );
  });
  it('Chart data OK with all number', async () => {
    await expect(createLineChart([1, 2, 3, 1, 4.1], 'Test')).resolves.toEqual(
      expect.stringContaining('.png'),
    );
  });
  it('Chart data OK with a number as string', async () => {
    await expect(createLineChart([1, 2, 3, 1, '4.0'], 'Test')).resolves.toEqual(
      expect.stringContaining('.png'),
    );
  });
});
