const { writeFileSync, existsSync, mkdirSync } = require('fs');
const { join } = require('path');
const { ChartJSNodeCanvas, ChartCallback } = require('chartjs-node-canvas');
const {
  GRAPH_LINE_DEFAULT_CONFIG,
  GRAPH_CANVAS_DEFAULT,
  TMP_PATH,
} = require('../constants');

function writeToFileSync(chartBuffer, bufferEncoding) {
  // create tmp folder if it doesn't exist
  if (!existsSync(TMP_PATH)) {
    mkdirSync(TMP_PATH, { recursive: true });
  }
  // The PNG file name with current time in microseconds
  const filePath = join(TMP_PATH, `${process.hrtime.bigint().toString()}.png`);
  // write to file
  writeFileSync(filePath, chartBuffer, bufferEncoding);
  return filePath;
}

/**
 * Create LineChart from data input, save it to tmp folder
 * @param {number[]} data Number array
 * @param {string} label Label of the chart
 * @param {object} options chart options
 * return path to graph image file
 */
async function createLineChart(data, label, options = {}) {
  // Validate data, if item data is not a number array, reject it.
  const isNotOK =
    !data || data.length === 0 || data.some((item) => isNaN(item));
  if (isNotOK) {
    throw new Error('Data is not a number array');
  }

  const chartJSNodeCanvas = new ChartJSNodeCanvas(GRAPH_CANVAS_DEFAULT);
  // final config for the chart
  const chartConfig = Object.assign(
    {},
    GRAPH_LINE_DEFAULT_CONFIG,
    {
      data: {
        // The y-axis is the data number array, the x-axis is the index of the number in the array
        labels: data.map((_, index) => index),
        datasets: [
          {
            label,
            data,
            // hard code
            borderWidth: 1,
            borderColor: 'rgb(75, 192, 192)',
          },
        ],
      },
    },
    options,
  );
  // draw the chart to buffer
  const buffer = await chartJSNodeCanvas.renderToBuffer(chartConfig);
  return writeToFileSync(buffer, 'base64');
}

module.exports = {
  createLineChart,
};
