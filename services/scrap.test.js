const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const {
  getContentFromUrl,
  getDataFromHTMLContent,
  getDataFromUrl,
} = require('./scrap');
const TEST_URL = 'https://test.com';

describe('getContentFromUrl', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });

  test('URL not found', async () => {
    mock.onGet(TEST_URL).reply(404);
    await expect(getContentFromUrl(TEST_URL)).rejects.toThrow();
  });
  test('URL 400', async () => {
    mock.onGet(TEST_URL).reply(400);
    await expect(getContentFromUrl(TEST_URL)).rejects.toThrow();
  });
  test('URL 500', async () => {
    mock.onGet(TEST_URL).reply(500);
    await expect(getContentFromUrl(TEST_URL)).rejects.toThrow();
  });
  test('Content OK', async () => {
    mock.onGet(TEST_URL).reply(200, '<html><body>ok</body></html>');
    await expect(getContentFromUrl(TEST_URL)).resolves.toContain('ok');
  });
});

describe('getDataFromHTMLContent', () => {
  test('Table not found', () => {
    const html = '<div>Example</div>';
    expect(() => getDataFromHTMLContent(html)).toThrow(
      'Data from HTML not found',
    );
  });
  test('Column Mark empty row', () => {
    const html =
      '<table><tr><th>Mark</th></tr><tr><td>1</td></tr><tr><td></td></tr></table>';
    expect(() => getDataFromHTMLContent(html)).toThrow(
      'One or more value in Mark column is not a number',
    );
  });
  test('Column Mark on first table', () => {
    const html =
      '<table><tr><th>Mark</th></tr><tr><td>1</td></tr><tr><td>2.1 abc</td></tr></table>';
    expect(getDataFromHTMLContent(html)).toEqual([1, 2.1]);
  });
  test('Column Mark second table', () => {
    const html =
      '<table><tr><th>Score</th></tr><tr><td>3</td></tr><tr><td>4 abc</td></tr></table><table><tr><th>Mark</th></tr><tr><td>1</td></tr><tr><td>2.1 abc</td></tr></table>';
    expect(getDataFromHTMLContent(html)).toEqual([1, 2.1]);
  });
});

describe('getDataFromUrl', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });

  test('Wrong data', async () => {
    mock
      .onGet(TEST_URL)
      .reply(200, '<html><head></head><body>test</body></html');
    await expect(getDataFromUrl(TEST_URL)).rejects.toThrow(
      'Data from HTML not found',
    );
  });

  test('Data OK', async () => {
    mock
      .onGet(TEST_URL)
      .reply(
        200,
        '<html><head></head><body><table><tr><th>Mark</th></tr><tr><td>1</td></tr><tr><td>2.1 abc</td></tr></table></body></html',
      );
    await expect(getDataFromUrl(TEST_URL)).resolves.toEqual([1, 2.1]);
  });
});
