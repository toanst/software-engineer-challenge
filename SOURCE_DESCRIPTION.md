# Source description

## Assumptions

- HTML content from URL is server-side rendering
- Find a numeric column, for simply assuming numeric columns has a header is "Mark"
- All rows in the numeric column must have a value
- The chart y-axis is the number array(extracted from HTML content), the x-axis is the index of the value in the array

## Installation

The source code using NodeJS (Tested with Node v16.15.1 on MacOS)

### Libraries

- axios: to get HTML content from the URL
- cheerio: parse HTML content to get the Data
- chart.js and chartjs-node-canvas: to draw the chart in NodeJs environment
- jest and axios-mock-adapter: for testing purposes

### Install dependencies

Locate to the root of the project and run the following command

```bash
npm install
```

### Run tests

```bash
npm run test
```

### Run the code

Locate to the root of the project and run the following command

```bash
node index <url>
```

Examples:

```bash
node index 'https://en.wikipedia.org/wiki/Women%27s_high_jump_world_record_progression'
```

```bash
node index 'https://en.wikipedia.org/wiki/Men%27s_high_jump_world_record_progression'
```
