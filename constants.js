const { join } = require('path');

module.exports = {
  GRAPH_CANVAS_DEFAULT: {
    width: 800,
    height: 600,
    backgroundColour: 'white',
  },
  GRAPH_LINE_DEFAULT_CONFIG: {
    type: 'line',
    options: {
      responsive: true,
    },
  },
  TMP_PATH: join(process.cwd(), 'tmp'),
};
