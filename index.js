const { createLineChart } = require('./services/graph');
const { getDataFromUrl } = require('./services/scrap');

// Assume the URL is correct
// HTML is server side rendering
const url = process.argv.length > 2 ? process.argv[2] : '';

function main() {
  if (!url) {
    console.log(
      "Please provide a URL, example command: node index 'https://en.wikipedia.org/wiki/Women%27s_high_jump_world_record_progression'",
    );
    return;
  }
  getDataFromUrl(url)
    .then((data) => createLineChart(data, url))
    .then((fileName) => console.log('Chart file created:', fileName))
    .catch(console.error);
}

main();
